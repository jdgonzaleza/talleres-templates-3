package taller.interfaz;

import java.util.Scanner;

import taller.mundo.LineaCuatro;

public class Interfaz 
{
	/**
	 * Juego actual
	 */
	private LineaCuatro juego;
	
	/**
	 * EscÃ¡ner de texto ingresado por el usuario en la consola
	 */
	private Scanner sc;
	
	/**
	 * Crea una nueva instancia de la clase de interacciÃ³n del usuario con la consola
	 */
	public Interfaz()
	{
		sc= new Scanner (System.in);
		while (true)
		{
			imprimirMenu();
			try
			{
				int opt=Integer.parseInt(sc.next());
				if(opt==1)
				{
					empezarJuego();
				}
				else if(opt==2)
				{
					empezarJuegoMaquina();
				}
				else if(opt==3)
				{
					System.out.println("Â¡Vuelva pronto!");
					break;
				}
				else
				{
					System.out.println("Comando invÃ¡lido");
					continue;
				}
			}
			catch (NumberFormatException e)
			{
				System.out.println("Comando invÃ¡lido");
				continue;
			}


		}
	}
	/**
	 * Imprime el menÃº principal
	 */
	public void imprimirMenu()
	{
		System.out.println("------------------------LÃ�NEA CUATRO------------------------");
		System.out.println("-MenÃº principal-");
		System.out.println("Ingrese un comando:");
		System.out.println("1. Empezar juego con amigos");
		System.out.println("2. Empezar juego vs mÃ¡quina");
		System.out.println("3. Salir");		
    }
	
	/**
	 * Crea un nuevo juego
	 */
	public void empezarJuego()
	{
       //TODO
	}
	
	/**
	 * Modera el juego entre jugadores
	 */
	public void juego()
	{
		//TODO
	}
	/**
	 * Empieza el juego contra la mÃ¡quina
	 */
	public void empezarJuegoMaquina()
	{
		//TODO
	}
	/**
	 * Modera el juego contra la mÃ¡quina
	 */
	public void juegoMaquina()
	{
		//TODO
	}

	/**
	 * Imprime el estado actual del juego
	 */
	public void imprimirTablero()
	{
		String[][] tablero = juego.darTablero();
		//TODO
		for(int i = 0; i < juego.darTablero().length; i++)
		{
			System.out.println();
			for(int j = 0; j< juego.darTablero().length; j++)
			{
				tablero = new String[i][j]; 
				if( j==0)
				{
					System.out.println("| ");
					System.out.println(tablero + " | ");
				}
			}
		}
		System.out.println();
	}
}
